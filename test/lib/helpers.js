const { promisify: p } = require('util')
const omit = require('lodash.omit')
const fixInput = require('../../src/lib/fix-input')

const getWhoami = {
  query: `query  {
    whoami {
      linkedProfileIds
      # linkedProfiles {
      #   id
      #   customFields {
      #     key
      #     value
      #   }
      #   recps
      # }
    }
  }`
}

const getCommunity = (id) => ({
  query: `query($id: String!) {
    community(id: $id) {
      id
      recps
      type
      preferredName
      description
      avatarImage { uri }
      headerImage { uri }

      joiningQuestions {
        type
        label
      }
      acceptsVerifiedCredentials
      issuesVerifiedCredentials
      poBoxId
      customFields {
        key
        type
        label
        order
        required
        visibleBy
        ...on CommunityCustomFieldList {
          options
          multiple
        }
        ...on CommunityCustomFieldFile {
          fileTypes
          description
          multiple
        }
      }

      address
      city
      country
      postCode
      phone
      email

      allowWhakapapaViews
      allowPersonsList
      allowStories
    }
  }`,
  variables: {
    id
  }
})

const getPerson = (id) => ({
  query: `query($id: String!) {
    person(id: $id) {
      id
      recps
      type

      preferredName
      altNames
      legalName
      description

      avatarImage { uri }
      headerImage { uri }

      address
      city
      country
      postCode
      phone
      email

      profession
      
      gender
      source
      aliveInterval
      birthOrder
      deceased
      placeOfBirth
      placeOfDeath
      buriedLocation

      education
      school

      customFields {
        key
        value
        ...on PersonCustomFieldDate {
          type
        }
      }
    }
  }`,
  variables: {
    id
  }
})

const getProfile = (id) => ({
  query: `query($id: String!) {
    profile(id: $id) {
      # shared profile fields
      id
      recps
      type
      
      preferredName
      description

      avatarImage { uri }
      headerImage { uri }
      
      address
      city
      country
      postCode
      phone
      email

      # community only fields
      ...on Community {
        joiningQuestions {
          type
          label
        }
        acceptsVerifiedCredentials
        issuesVerifiedCredentials
        poBoxId
        customFields {
          key
          type
          label
          order
          required
          visibleBy
          ...on CommunityCustomFieldList {
            multiple
            options
          }
        }

        allowWhakapapaViews
        allowPersonsList
        allowStories
      }

      # person only fields
      ...on Person {
        legalName
        altNames
        profession
        gender
        source
        aliveInterval
        birthOrder
        deceased
        placeOfBirth
        placeOfDeath
        buriedLocation
        education
        school
        customFields {
          key
          value
          ...on PersonCustomFieldDate {
            type
          }
        }
      }

    }
  }`,
  variables: {
    id
  }
})

function handleErr (err) {
  console.log(JSON.stringify(err, null, 2))
  return ({ errors: [err], err })
}

let i = 0

const generateTimestamp = () => {
  return (Date.now() + (i += 10)).toString()
}

function SavePerson (apollo) {
  return async function (input) {
    return apollo.mutate({
      mutation: `mutation($input: PersonProfileInput!) {
        savePerson(input: $input)
      }`,
      variables: { input }
    })
      .catch(handleErr)
  }
}

function DeletePerson (apollo) {
  return async function (id, tombstoneInput) {
    return apollo.mutate({
      mutation: `mutation($id: String!, $tombstoneInput: TombstoneInput) {
        deletePerson(id: $id, tombstoneInput: $tombstoneInput)
      }`,
      variables: { id, tombstoneInput }
    })
      .catch(handleErr)
  }
}

function SaveCommunity (apollo) {
  return async function (input) {
    return apollo.mutate({
      mutation: `mutation($input: CommunityProfileInput!) {
        saveCommunity(input: $input)
      }`,
      variables: { input }
    })
      .catch(handleErr)
  }
}

function GetPerson (apollo, query) {
  return async function (id) {
    return apollo.query(
      query ? query(id) : getPerson(id)
    )
      .catch(handleErr)
  }
}

function GetCommunity (apollo) {
  return async function (id) {
    return apollo.query(
      getCommunity(id)
    )
      .catch(handleErr)
  }
}

function GetWhoami (apollo) {
  return async function () {
    return apollo.query(getWhoami)
      .catch(handleErr)
  }
}

function InitGroup (ssb) {
  const privateOnlyFields = ['allowWhakapapaViews', 'allowStories', 'allowPersonsList']
  const publicOnlyFields = ['joiningQuestions', 'customFields', 'acceptsVerifiedCredentials', 'issuesVerifiedCredentials']

  async function initGroupProfiles (groupId, input) {
    // auto set authors to the creator
    const details = {
      ...fixInput.community(input), // includes poBoxId
      authors: {
        add: [ssb.id]
      }
    }

    // create the groups public profile
    const groupPublicProfileId = await p(ssb.profile.community.public.create)(omit(details, privateOnlyFields))
    await p(ssb.profile.link.create)(groupPublicProfileId, { groupId })

    // // create the groups private profile
    const groupProfileId = await p(ssb.profile.community.group.create)({ ...omit(details, publicOnlyFields), recps: [groupId] })
    await p(ssb.profile.link.create)(groupProfileId, { groupId })

    return { public: groupPublicProfileId, group: groupProfileId }
  }

  return async function initGroup (input) {
    const { groupId } = await p(ssb.tribes.create)({})
    // make a profile for you in this group

    let details = {
      authors: {
        add: [ssb.id]
      },
      recps: [groupId]
    }

    // init person group profile
    const groupProfileId = await p(ssb.profile.person.group.create)(details)
    await p(ssb.profile.link.create)(groupProfileId)

    // init admin subgroup
    const { poBoxId, groupId: adminGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true })

    details = {
      authors: {
        add: [ssb.id, '*']
      },
      recps: [poBoxId, ssb.id]
    }

    // init person admin profile
    const adminProfileId = await p(ssb.profile.person.admin.create)(details)
    await p(ssb.profile.link.create)(adminProfileId)

    input.poBoxId = poBoxId

    const communityProfiles = await initGroupProfiles(groupId, input)

    return {
      groupId,
      adminGroupId,
      poBoxId,
      community: {
        public: communityProfiles.public,
        group: communityProfiles.group
      },
      person: {
        group: groupProfileId,
        admin: adminProfileId
      }
    }
  }
}

function sleep (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

module.exports = {
  sleep,
  generateTimestamp,
  getCommunity,
  getPerson,
  getProfile,

  // apollo
  SavePerson,
  DeletePerson,
  SaveCommunity,
  GetPerson,
  GetCommunity,
  GetWhoami,

  // other
  InitGroup
}
