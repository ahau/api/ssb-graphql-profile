const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')
const TestBot = require('../../../test-bot')
const { GetCommunity, getProfile, SaveCommunity, handleErr } = require('../../../lib/helpers')

const MB = 1024 * 1024

test('saveCommunity (public profile/community)', async (t) => {
  t.plan(7)
  const { ssb, apollo } = await TestBot()

  const saveCommunity = SaveCommunity(apollo)
  const getCommunity = GetCommunity(apollo)

  const { groupId } = await p(ssb.tribes.create)({})
  const { poBoxId } = await p(ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true })

  const initial = await saveCommunity({
    type: 'community',

    authors: {
      add: [ssb.id]
    },

    preferredName: 'Whanau (community)',

    poBoxId,
    joiningQuestions: [
      { type: 'input', label: 'Ko wai tō ingoa?' },
      { type: 'input', label: 'Kei te pewhea koe?' },
      { type: 'textarea', label: 'Ko wai tō pēpehā?' }
    ],
    acceptsVerifiedCredentials: true,
    issuesVerifiedCredentials: true
  })

  t.error(initial.errors, 'no errors when creating a community')

  const profileId = initial.data.saveCommunity
  t.true(isMsgId(profileId), 'saves a public profile')

  const initialGet = await getCommunity(profileId)

  t.deepEqual(
    initialGet.data.community,
    {
      type: 'community',
      id: profileId,

      preferredName: 'Whanau (community)',
      description: null,

      avatarImage: null,
      headerImage: null,

      joiningQuestions: [
        { type: 'input', label: 'Ko wai tō ingoa?' },
        { type: 'input', label: 'Kei te pewhea koe?' },
        { type: 'textarea', label: 'Ko wai tō pēpehā?' }
      ],

      address: null,
      city: null,
      country: null,
      postCode: null,
      phone: null,
      email: null,
      poBoxId,

      allowPersonsList: true,
      allowWhakapapaViews: true,
      allowStories: true,
      customFields: [],
      acceptsVerifiedCredentials: true,
      issuesVerifiedCredentials: true,

      recps: null
    },
    'can retrieve public community profile'
  )

  /* update */
  const update = await saveCommunity({
    id: profileId, // << this makes it an update

    joiningQuestions: [
      { type: 'input', label: 'Ko wai tō ingoa?' },
      { type: 'input', label: 'Kei te pewhea koe?' },
      { type: 'textarea', label: 'Where do you stay?' }
      // remove other one and replace it with this one
    ],
    acceptsVerifiedCredentials: false,
    issuesVerifiedCredentials: false
  })

  t.error(update.errors, 'can update the community without errors')

  const updatedGet = await getCommunity(profileId)

  const profileRes = await apollo.query(getProfile(profileId))
    .catch(handleErr)

  t.error(profileRes.errors, 'gets profile without error')

  const expectedCommunity = {
    type: 'community',
    id: profileId,

    preferredName: 'Whanau (community)',
    description: null,

    avatarImage: null,
    headerImage: null,

    address: null,
    city: null,
    country: null,
    postCode: null,
    phone: null,
    email: null,
    poBoxId,

    allowPersonsList: true,
    allowStories: true,
    allowWhakapapaViews: true,

    customFields: [],
    acceptsVerifiedCredentials: false,
    issuesVerifiedCredentials: false,
    joiningQuestions: [
      { type: 'input', label: 'Ko wai tō ingoa?' },
      { type: 'input', label: 'Kei te pewhea koe?' },
      { type: 'textarea', label: 'Where do you stay?' }
    ],

    recps: null
  }

  t.deepEqual(
    profileRes.data.profile,
    expectedCommunity,
    'can get the public community profile with updated joining questions'
  )

  t.deepEqual(
    updatedGet.data.community,
    expectedCommunity,
    'can update profile'
  )

  ssb.close()
})

test('saveCommunity (group profile/community)', async (t) => {
  t.plan(7)
  const { ssb, apollo } = await TestBot()

  const saveCommunity = SaveCommunity(apollo)
  const getCommunity = GetCommunity(apollo)

  const { groupId } = await p(ssb.tribes.create)({})
  const { poBoxId } = await p(ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true })

  const initial = await saveCommunity({
    type: 'community',

    authors: {
      add: [ssb.id]
    },

    preferredName: 'Whanau (community)',
    description: 'This is the whanau tribe for the eriepa + clarke whanau',

    allowWhakapapaViews: true,
    allowPersonsList: false,
    // allowStories: null,

    avatarImage: {
      blob: '&CLbw5B9d5+H59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI=.sha256',
      unbox: '4bOkwCLbw5B9d5+H59oxDNOyIaOhfLfqOLm1MGKyTLI=.boxs',
      mimeType: 'image/png',
      size: 4 * MB,
      width: 500,
      height: 480
    },
    headerImage: {
      blob: '&CLbw5B9d5+H59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI=.sha256',
      mimeType: 'image/png'
    },

    phone: '02712345678',
    email: 'whanau@email.com',

    poBoxId,
    // acceptsVerifiedCredentials: null,
    // issuesVerifiedCredentials: null,

    recps: [groupId]
  })

  t.error(initial.errors, 'create inital community without errors')

  const profileId = initial.data.saveCommunity
  t.true(isMsgId(profileId), 'saves a fully featured profile, returning profileId')

  const initialGet = await getCommunity(profileId)

  t.deepEqual(
    initialGet.data.community,
    {
      type: 'community',
      id: profileId,

      preferredName: 'Whanau (community)',
      description: 'This is the whanau tribe for the eriepa + clarke whanau',

      avatarImage: {
        uri: 'http://localhost:26835/get/%26CLbw5B9d5%2BH59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI%3D.sha256?unbox=4bOkwCLbw5B9d5%2BH59oxDNOyIaOhfLfqOLm1MGKyTLI%3D.boxs'
      },
      headerImage: {
        uri: 'http://localhost:26835/get/%26CLbw5B9d5%2BH59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI%3D.sha256'
      },

      address: null,
      city: null,
      country: null,
      postCode: null,
      phone: '02712345678',
      email: 'whanau@email.com',
      poBoxId,

      allowWhakapapaViews: true,
      allowPersonsList: false,
      allowStories: true,

      joiningQuestions: null,
      customFields: null,
      acceptsVerifiedCredentials: false,
      issuesVerifiedCredentials: false,

      recps: [groupId]
    },
    'can retrieve group community profile'
  )

  // /* update */
  const update = await saveCommunity({
    id: profileId, // << this makes it an update

    allowStories: false
  })
  t.error(update.errors, 'update community without error')

  const updatedGet = await getCommunity(profileId)

  const profileRes = await apollo.query(getProfile(profileId))
    .catch(handleErr)

  t.error(profileRes.errors, 'gets profile without error')

  const expectedCommunity = {
    id: profileId,
    type: 'community',
    recps: [groupId],
    preferredName: 'Whanau (community)',
    description: 'This is the whanau tribe for the eriepa + clarke whanau',
    avatarImage: { uri: 'http://localhost:26835/get/%26CLbw5B9d5%2BH59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI%3D.sha256?unbox=4bOkwCLbw5B9d5%2BH59oxDNOyIaOhfLfqOLm1MGKyTLI%3D.boxs' },
    headerImage: { uri: 'http://localhost:26835/get/%26CLbw5B9d5%2BH59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI%3D.sha256' },
    address: null,
    city: null,
    country: null,
    postCode: null,
    phone: '02712345678',
    email: 'whanau@email.com',
    poBoxId,

    allowWhakapapaViews: true,
    allowPersonsList: false,
    allowStories: false,

    // only on public community profiles
    joiningQuestions: null,
    customFields: null,
    acceptsVerifiedCredentials: false,
    issuesVerifiedCredentials: false
  }

  t.deepEqual(
    profileRes.data.profile,
    expectedCommunity,
    'can query using profile'
  )

  delete expectedCommunity.legalName
  delete expectedCommunity.altNames

  t.deepEqual(
    updatedGet.data.community,
    expectedCommunity,
    'can update profile'
  )

  ssb.close()
})
