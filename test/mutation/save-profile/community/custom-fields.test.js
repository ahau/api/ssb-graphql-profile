const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')
const TestBot = require('../../../test-bot')
const sortAscending = require('../../../../src/lib/sort-ascending')
const { SaveCommunity, GetCommunity, generateTimestamp } = require('../../../lib/helpers')

const emptyFields = {
  type: 'community',
  preferredName: 'Whanau (community)',
  description: null,
  avatarImage: null,
  headerImage: null,
  joiningQuestions: [],
  acceptsVerifiedCredentials: false,
  issuesVerifiedCredentials: false,
  address: null,
  city: null,
  country: null,
  postCode: null,
  phone: null,
  email: null,
  allowPersonsList: true,
  allowWhakapapaViews: true,
  allowStories: true,
  recps: null
}

test('saveCommunity (public profile/community) (customFields)', async (t) => {
  t.plan(7)
  const { ssb, apollo } = await TestBot()

  const saveCommunity = SaveCommunity(apollo)
  const getCommunity = GetCommunity(apollo)

  const { groupId } = await p(ssb.tribes.create)({})
  const { poBoxId } = await p(ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true })

  // NOTE: each data field will use a timestamp as the key
  const timestampA = generateTimestamp()
  const timestampB = generateTimestamp()
  const timestampC = generateTimestamp()
  const timestampD = generateTimestamp()
  const timestampE = generateTimestamp()
  const timestampF = generateTimestamp()
  const timestampG = generateTimestamp()
  const timestampH = generateTimestamp()
  const timestampI = generateTimestamp()

  // NOTE: graphql will handle converting this to the appropriate format for ssb
  const customFields = [
    {
      key: timestampA,
      type: 'text',
      label: 'Hometown',
      order: 1.1,
      required: false,
      visibleBy: 'members'
    },
    {
      key: timestampB,
      type: 'array',
      label: 'Qualifications',
      required: false,
      visibleBy: 'members'
    },
    {
      key: timestampC,
      type: 'list',
      label: 'Employement Status',
      required: false,
      multiple: false,
      order: 2.2,
      options: ['Employed', 'Unemployed', 'Self-emplyed', 'Student'],
      visibleBy: 'members'
    },
    {
      key: timestampD,
      type: 'checkbox',
      label: 'Living in NZ',
      order: 3,
      required: false,
      visibleBy: 'admin'
    },
    {
      key: timestampE,
      type: 'number',
      label: 'Age',
      order: 4.5,
      required: false,
      visibleBy: 'admin'
    },
    {
      key: timestampF,
      type: 'number',
      label: 'Shares',
      order: 5,
      required: false,
      visibleBy: 'admin'
    },
    {
      key: timestampG,
      type: 'date',
      label: 'Date succeeded',
      order: 6,
      required: false,
      visibleBy: 'members'
    },
    {
      key: timestampH,
      type: 'file',
      fileTypes: ['document'],
      label: 'Registration Form',
      description: 'Upload your registration form here...',
      multiple: false,
      order: 7,
      required: true,
      visibleBy: 'admin'
    },
    {
      key: timestampI,
      type: 'file',
      fileTypes: ['photo'],
      label: 'Self Portrait Photos',
      description: 'Upload your photos here...',
      multiple: true,
      order: 8,
      required: false,
      visibleBy: 'members'
    }
  ]

  const initial = await saveCommunity({
    type: 'community',
    authors: {
      add: [ssb.id]
    },
    preferredName: 'Whanau (community)',
    customFields,
    poBoxId
  })

  t.error(initial.errors, 'save community doesnt return errors')

  const profileId = initial.data.saveCommunity
  t.true(isMsgId(profileId), 'saves a public profile')

  const initialGet = await getCommunity(profileId)

  t.error(initialGet.errors, 'get community doesnt return errors')

  customFields[1].order = null

  t.deepEqual(
    initialGet.data.community.customFields,
    customFields.sort(sortAscending),
    'can retrieve correct custom fields'
  )

  const timestampJ = generateTimestamp()

  const updatedCustomFields = [
    // update an existing
    {
      key: timestampA,
      type: 'text',
      label: 'Hometown',
      order: 1,
      required: true,
      visibleBy: 'admin'
    },
    // update a list field
    {
      key: timestampC,
      type: 'list',
      label: 'Employement Status',
      multiple: false,
      required: false,
      order: 2,
      options: ['Employed', 'Unemployed', 'Self-Employed', 'Student'],
      visibleBy: 'members'
    },
    // add a new one
    {
      key: timestampJ,
      type: 'text',
      label: 'Social Media Links',
      order: 10.12,
      required: false,
      visibleBy: 'members'
    },

    // tombstone one
    {
      key: timestampF,
      type: 'number',
      label: 'Shares',
      order: 5,
      required: false,
      visibleBy: 'admin',
      tombstone: {
        date: Date.now(),
        reason: 'user deleted field'
      }
    }
  ]

  // /* update */
  const update = await saveCommunity({
    id: profileId, // << this makes it an update
    customFields: updatedCustomFields
  })

  t.error(update.errors, 'update the community without errors')

  const updatedGet = await getCommunity(profileId)

  t.error(updatedGet.errors, 'gets updated community without errors')

  const expectedCommunity = {
    id: profileId,
    poBoxId,
    ...emptyFields,
    customFields: [
      {
        key: timestampA,
        type: 'text',
        label: 'Hometown',
        order: 1,
        required: true,
        visibleBy: 'admin'
      },
      // update a list field
      {
        key: timestampC,
        type: 'list',
        label: 'Employement Status',
        required: false,
        multiple: false,
        order: 2,
        options: ['Employed', 'Unemployed', 'Self-Employed', 'Student'],
        visibleBy: 'members'
      },
      // add a new one
      {
        key: timestampJ,
        type: 'text',
        label: 'Social Media Links',
        order: 10.12,
        required: false,
        visibleBy: 'members'
      },
      {
        key: timestampB,
        type: 'array',
        label: 'Qualifications',
        order: null,
        required: false,
        visibleBy: 'members'
      },
      {
        key: timestampD,
        type: 'checkbox',
        label: 'Living in NZ',
        order: 3,
        required: false,
        visibleBy: 'admin'
      },
      {
        key: timestampE,
        type: 'number',
        label: 'Age',
        order: 4.5,
        required: false,
        visibleBy: 'admin'
      },
      { // tombstoned!
        key: timestampF,
        type: 'number',
        label: 'Shares',
        order: 5,
        required: false,
        visibleBy: 'admin'
      },
      {
        key: timestampG,
        type: 'date',
        label: 'Date succeeded',
        order: 6,
        required: false,
        visibleBy: 'members'
      },
      {
        key: timestampH,
        type: 'file',
        fileTypes: ['document'],
        label: 'Registration Form',
        description: 'Upload your registration form here...',
        multiple: false,
        order: 7,
        required: true,
        visibleBy: 'admin'
      },
      {
        key: timestampI,
        type: 'file',
        fileTypes: ['photo'],
        label: 'Self Portrait Photos',
        description: 'Upload your photos here...',
        multiple: true,
        order: 8,
        required: false,
        visibleBy: 'members'
      }
    ]
  }

  t.deepEqual(
    updatedGet.data.community.customFields,
    expectedCommunity.customFields.sort(sortAscending),
    'can get the public community profile with updated custom fields'
  )

  ssb.close()
})
