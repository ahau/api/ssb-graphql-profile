const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../../../test-bot')
const { SavePerson, GetPerson, InitGroup, sleep, generateTimestamp } = require('../../../lib/helpers')

const keyA = generateTimestamp()
const keyB = generateTimestamp()
const keyC = generateTimestamp()
const keyD = generateTimestamp()
const keyE = generateTimestamp()

const communityInput = () => ({
  preferredName: 'Smoothie Club',
  // these are the custom field definitions
  customFields: [
    {
      key: keyA,
      type: 'text',
      label: 'Favourite Smoothie',
      order: 1,
      required: false,
      visibleBy: 'members'
    },
    {
      key: keyB,
      type: 'text',
      label: 'Membership Status',
      order: 1,
      required: false,
      visibleBy: 'admin'
    },
    {
      key: keyC,
      type: 'text',
      label: 'Membership ID',
      order: 1,
      required: false,
      visibleBy: 'admin'
    },
    {
      key: keyD,
      type: 'file',
      fileTypes: ['document'],
      label: 'Registration form',
      description: 'Upload your registration form',
      order: 1,
      required: false,
      visibleBy: 'admin',
      multiple: false
    },
    {
      key: keyE,
      type: 'file',
      fileTypes: ['photo'],
      label: 'Proof of ID',
      description: 'Upload your proof of ID',
      order: 1,
      required: true,
      visibleBy: 'admin',
      multiple: true
    }
  ]
})

const blobDocument = { // blob for pdf file
  type: 'ssb',
  blobId: '&wCfZQM7TjQHBUEcdB4aXcjF1ijB6y7dkX047plTjrJo=.sha256',
  mimeType: 'application/pdf',
  size: 12345,
  unbox: 'XfNYmNz1594ZfE/JUhPw/xkjopYiGyOOQwNDf6DN+54=.boxs'
}

const blobPhotoA = { // blob for pdf file
  type: 'ssb',
  blobId: '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256',
  mimeType: 'image/jpeg',
  size: 12345,
  unbox: 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs'
}

const blobPhotoB = {
  type: 'hyper',
  driveAddress: 'T1WauuJ6LJWUA0mT+aVf5C5UgCk9pV5fFH4uhw2PeVU=',
  blobId: '0c2b3df4-77e5-40af-99ad-5ff62d857d35',
  readKey: 'Wenusia9d1gdhJNGsgY+66xRjgbl82N7daFeaM436qs=',
  mimeType: 'image/png',
  size: 123454
}

const getPersonQuery = (id) => ({
  query: `query($id: String!) {
    person(id: $id) {
      id
      recps
    
      customFields {
        key
        value
        ...on PersonCustomFieldDate {
          type
        }
      }

      adminProfile {
        customFields {
          key
          value
          ...on PersonCustomFieldDate {
            type
          }
        }
      }
    }
  }`,
  variables: {
    id
  }
})

test('updating custom fields (group owned)', async t => {
  t.plan(12)

  /**
   * SETUP
   */
  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  const initGroup = InitGroup(kaitiaki.ssb)

  const savePersonMember = SavePerson(member.apollo)
  const getPersonMember = GetPerson(member.apollo, getPersonQuery)

  const savePersonKaitiaki = SavePerson(kaitiaki.apollo)
  const getPersonKaitiaki = GetPerson(kaitiaki.apollo, getPersonQuery)

  // 1. admin creates a group
  const { groupId, /* adminSubGroupId, */ poBoxId } = await initGroup(communityInput())

  // 2. the member creates their admin profile in the group
  let details = {
    authors: {
      add: [member.id, '*']
    },
    recps: [poBoxId, member.id]
  }
  const adminProfileId = await p(member.ssb.profile.person.admin.create)(details)
  await p(member.ssb.profile.link.create)(adminProfileId) // links it to ourself
  await member.replicate(kaitiaki)

  // 3. the kaitiaki invites the member to join and creates their group profile and links it to the member, and the members admin profile
  details = {
    authors: {
      add: [member.id]
    },
    recps: [groupId]
  }
  const groupProfileId = await p(kaitiaki.ssb.profile.person.group.create)(details)
  await p(kaitiaki.ssb.profile.link.create)(groupProfileId, { feedId: member.id })
  await p(kaitiaki.ssb.profile.link.create)(groupProfileId, { profileId: adminProfileId })
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.ssb.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // wait a second because the db rebuilds when replicated to the member
  await sleep(1000)

  // /**
  //  * TESTS
  //  */

  // 1. the member gets their profile and sees no custom fields
  let res = await getPersonMember(groupProfileId)
  t.deepEqual(res.data.person.adminProfile.customFields, [], 'member has no custom fields on their admin profile')
  t.deepEqual(res.data.person.customFields, [], 'member has no custom fields on their group profile')

  // 2. the kaitiaki gets the members profile and sees no custom fields either
  res = await getPersonKaitiaki(groupProfileId)
  t.deepEqual(res.data.person.adminProfile.customFields, [], 'kaitiaki sees the member has no custom fields on their admin profile')
  t.deepEqual(res.data.person.customFields, [], 'kaitiaki sees the member has no custom fields on their group profile')

  // 3. member updates their profile
  res = await savePersonMember({
    id: groupProfileId,
    customFields: [
      {
        key: keyA,
        value: 'Mango Smoothie'
      },
      {
        key: keyB,
        value: 'pending'
      },
      {
        key: keyC,
        value: '123123123123'
      },
      {
        key: keyD,
        value: blobDocument
      },
      {
        key: keyE,
        value: [blobPhotoA, blobPhotoB]
      }
    ]
  })
  await member.replicate(kaitiaki)
  await sleep(500)

  // 4. member gets their profile
  res = await getPersonMember(groupProfileId)

  if (res.errors) console.log(JSON.stringify(res.errors, null, 2))
  t.deepEqual(
    res.data.person.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      }
    ],
    'member sees correct custom fields on the group profile'
  )
  t.deepEqual(
    res.data.person.adminProfile.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      },
      {
        key: keyB,
        value: 'pending'
      },
      {
        key: keyC,
        value: '123123123123'
      },
      {
        key: keyD,
        value: blobDocument
      },
      {
        key: keyE,
        value: [blobPhotoA, blobPhotoB]
      }
    ],
    'member sees correct custom fields on the admin profile'
  )

  // 5. kaitiaki gets the members profile
  res = await getPersonKaitiaki(groupProfileId)
  t.deepEqual(
    res.data.person.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      }
    ],
    'kaitiaki sees correct custom fields on the members group profile'
  )
  t.deepEqual(
    res.data.person.adminProfile.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      },
      {
        key: keyB,
        value: 'pending'
      },
      {
        key: keyC,
        value: '123123123123'
      },
      {
        key: keyD,
        value: blobDocument
      },
      {
        key: keyE,
        value: [blobPhotoA, blobPhotoB]
      }
    ],
    'kaitiaki sees the correct custom fields on the members admin profile'
  )

  // 6. kaitiaki updates the members profile
  res = await savePersonKaitiaki({
    id: groupProfileId,
    customFields: [
      {
        key: keyB,
        value: 'verified'
      },
      {
        key: keyE,
        value: [blobPhotoB]
      }
    ]
  })
  await kaitiaki.replicate(member)
  await sleep(500)

  // 7. member gets their profile
  res = await getPersonMember(groupProfileId)
  t.deepEqual(
    res.data.person.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      }
    ],
    'member sees correct custom fields on the group profile'
  )
  t.deepEqual(
    res.data.person.adminProfile.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      },
      {
        key: keyB,
        value: 'verified'
      },
      {
        key: keyC,
        value: '123123123123'
      },
      {
        key: keyD,
        value: blobDocument
      },
      {
        key: keyE,
        value: [blobPhotoB]
      }
    ],
    'member sees correct custom fields on the admin profile'
  )

  // 7. kaitiaki gets the members profile
  res = await getPersonKaitiaki(groupProfileId)
  t.deepEqual(
    res.data.person.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      }
    ],
    'kaitiaki sees correct custom fields on the members group profile'
  )
  t.deepEqual(
    res.data.person.adminProfile.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      },
      {
        key: keyB,
        value: 'verified'
      },
      {
        key: keyC,
        value: '123123123123'
      },
      {
        key: keyD,
        value: blobDocument
      },
      {
        key: keyE,
        value: [blobPhotoB]
      }
    ],
    'kaitiaki sees the correct custom fields on the members admin profile'
  )

  kaitiaki.close()
  member.close()
})

test('kaitiaki updates custom fields (group unowned)', async t => {
  t.plan(6)

  /**
   * SETUP
   */
  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const initGroup = InitGroup(kaitiaki.ssb)
  const savePerson = SavePerson(kaitiaki.apollo)
  const getPerson = GetPerson(kaitiaki.apollo, getPersonQuery)

  // create the group with custom field definitions
  const { groupId } = await initGroup(communityInput())

  // /**
  //  * TESTS
  //  */

  // 1. the member creates an unowned profile with custom fields
  let res = await savePerson({
    type: 'person',
    customFields: [
      {
        key: keyA,
        value: 'Mango Smoothie'
      },
      {
        key: keyB,
        value: 'pending'
      },
      {
        key: keyC,
        value: '123123123123'
      },
      {
        key: keyD,
        value: blobDocument
      },
      {
        key: keyE,
        value: [blobPhotoA, blobPhotoB]
      }
    ],
    authors: {
      add: [kaitiaki.id]
    },
    recps: [groupId]
  })
  t.error(res.errors, 'creates profile without errors')

  const profileId = res.data.savePerson

  // 2. the member gets the profile
  res = await getPerson(profileId)
  t.error(res.errors, 'gets profile without errors')
  t.deepEqual(
    res.data.person,
    {
      id: profileId,
      recps: [groupId],
      customFields: [
        {
          key: keyA,
          value: 'Mango Smoothie'
        }
      ],
      adminProfile: {
        customFields: [
          {
            key: keyA,
            value: 'Mango Smoothie'
          },
          {
            key: keyB,
            value: 'pending'
          },
          {
            key: keyC,
            value: '123123123123'
          },
          {
            key: keyD,
            value: blobDocument
          },
          {
            key: keyE,
            value: [blobPhotoA, blobPhotoB]
          }
        ]
      }
    },
    'correct custom fields saved to the correct profiles'
  )

  // 3. the member updates the profile
  res = await savePerson({
    id: profileId,
    customFields: [
      {
        key: keyB,
        value: 'verified'
      },
      {
        key: keyE,
        value: [blobPhotoA]
      }
    ]
  })
  t.error(res.errors, 'updates profile without error')

  // 4. the member gets the profile
  res = await getPerson(profileId)
  t.error(res.errors, 'gets updated profile without errors')
  t.deepEqual(
    res.data.person,
    {
      id: profileId,
      recps: [groupId],
      customFields: [
        {
          key: keyA,
          value: 'Mango Smoothie'
        }
      ],
      adminProfile: {
        customFields: [
          {
            key: keyA,
            value: 'Mango Smoothie'
          },
          {
            key: keyB,
            value: 'verified'
          },
          {
            key: keyC,
            value: '123123123123'
          },
          {
            key: keyD,
            value: blobDocument
          },
          {
            key: keyE,
            value: [blobPhotoA]
          }
        ]
      }
    },
    'updated custom fields saved to the correct profiles'
  )

  kaitiaki.close()
})

test('member updating custom fields (group unowned)', async t => {
  t.plan(10)

  /**
   * SETUP
   */
  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  const initGroup = InitGroup(kaitiaki.ssb)

  const savePersonMember = SavePerson(member.apollo)
  const getPersonMember = GetPerson(member.apollo, getPersonQuery)

  const savePersonKaitiaki = SavePerson(kaitiaki.apollo)
  const getPersonKaitiaki = GetPerson(kaitiaki.apollo, getPersonQuery)

  // 1. admin creates a group
  const { groupId } = await initGroup(communityInput())

  // 2. the kaitiaki invites the member to join
  // NOTE: the member doesnt need any profiles in the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.ssb.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)

  // wait a second because the db rebuilds when replicated to the member
  await sleep(1000)

  // /**
  //  * TESTS
  //  */

  // 1. the kaitiaki creates an unowned profile
  let res = await savePersonKaitiaki({
    type: 'person',
    preferredName: 'Alice',
    customFields: [
      {
        key: keyB,
        value: 'not applicable'
      },
      {
        key: keyC,
        value: 'not applicable'
      },
      {
        key: keyD,
        value: blobDocument
      },
      {
        key: keyE,
        value: [blobPhotoA, blobPhotoB]
      }
    ],
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })
  t.error(res.errors, 'kaitiaki creates an unowned profile without errors')

  await kaitiaki.replicate(member)

  const profileId = res.data.savePerson

  // 2. the kaitiaki gets the unowned profile
  res = await getPersonKaitiaki(profileId)
  t.error(res.errors, 'the kaitiaki gets the unowned profile without errors')
  t.deepEqual(
    res.data.person.adminProfile.customFields,
    [
      {
        key: keyB,
        value: 'not applicable'
      },
      {
        key: keyC,
        value: 'not applicable'
      },
      {
        key: keyD,
        value: blobDocument
      },
      {
        key: keyE,
        value: [blobPhotoA, blobPhotoB]
      }
    ],
    'kaitiaki sees the profile has no custom fields on their admin profile'
  )

  t.deepEqual(res.data.person.customFields, [], 'kaitiaki sees the profile has no custom fields on their group profile')

  // 3. member updates the unowned profile
  res = await savePersonMember({
    id: profileId,
    customFields: [
      {
        key: keyA,
        value: 'Mango Smoothie'
      }
    ]
  })
  t.error(res.errors, 'member updates the unowned profile without errors')

  await member.replicate(kaitiaki)
  await sleep(500)

  // 4. member gets the unowned profile
  res = await getPersonMember(profileId)
  t.error(res.errors, 'member gets the unowned profile without errors')
  t.deepEqual(
    res.data.person.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      }
    ],
    'member sees correct custom fields on the unowned profile'
  )
  t.deepEqual(
    res.data.person.adminProfile,
    null,
    'member sees no admin profile'
  )

  // 5. kaitiaki gets the unowned profile
  res = await getPersonKaitiaki(profileId)
  t.deepEqual(
    res.data.person.customFields,
    [
      {
        key: keyA,
        value: 'Mango Smoothie'
      }
    ],
    'kaitiaki sees correct custom fields on the members group profile'
  )

  // NOTE: the changes submitted by the member are discarded because they cannot
  // make changes to an unowned admin profile
  t.deepEqual(
    res.data.person.adminProfile.customFields,
    [
      {
        key: keyB,
        value: 'not applicable'
      },
      {
        key: keyC,
        value: 'not applicable'
      },
      {
        key: keyD,
        value: blobDocument
      },
      {
        key: keyE,
        value: [blobPhotoA, blobPhotoB]
      }
    ],
    'kaitiaki sees the correct custom fields on the members admin profile'
  )

  kaitiaki.close()
  member.close()
})

/*
  NOTE: writing this test as I get an error when i"
  1. Have a group and an admin subgroup
  2. Save profiles in the admin subgroup with custom fields?
*/
test('kaitiaki updating custom fields (admin group unowned)', async t => {
  t.plan(3)
  // t.end()

  /**
   * SETUP
   */
  const kaitiaki = await TestBot({ name: 'kaitiaki' })

  const initGroup = InitGroup(kaitiaki.ssb)

  const savePerson = SavePerson(kaitiaki.apollo)
  const getPerson = GetPerson(kaitiaki.apollo, getPersonQuery)

  // 1. admin creates a group
  const communityInput = {
    preferredName: 'Smoothie Club',
    // these are the custom field definitions
    customFields: [
      {
        key: '1678228069813',
        label: 'secret',
        type: 'text',
        required: false,
        visibleBy: 'admin',
        tombstone: null
      },
      {
        key: '1678228069823',
        label: 'hometown',
        type: 'text',
        required: false,
        visibleBy: 'members',
        tombstone: null
      },
      {
        key: keyE,
        type: 'file',
        fileTypes: ['photo'],
        label: 'Your files',
        description: 'Upload your files',
        order: 1,
        required: true,
        visibleBy: 'admin',
        multiple: true
      }
    ]
  }
  const { adminGroupId } = await initGroup(communityInput)

  // kaitiaki makes profiles in the admin subgroup
  const profileInput = {
    type: 'person',
    authors: {
      add: [
        '*'
      ]
    },
    customFields: [
      {
        key: '1678228069813',
        value: 'this is a secret?'
      },
      {
        key: '1678228069823',
        value: 'Hamilton'
      },
      {
        key: keyE,
        value: [blobDocument, blobPhotoA, blobPhotoB]
      }
    ],
    preferredName: 'Cherese',
    legalName: 'Cherese Eriepa',
    gender: 'female',
    source: 'ahau',
    city: 'Hamilton',
    country: 'New Zealand',
    profession: 'Software Engineer',
    aliveInterval: '/',
    birthOrder: 2,
    placeOfBirth: 'Waikato Hospital',
    recps: [adminGroupId]
  }

  // save the profile
  let res = await savePerson(profileInput)
  t.error(res.errors, 'saves person without error')

  // get the profile
  const profileId = res.data.savePerson

  res = await getPerson(profileId)
  t.error(res.errors, 'gets person without error')

  const profile = res.data.person

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: null, // expected to be null because this is already an adminProfile itself
      recps: [adminGroupId],

      // we expect all of the custom fields to be saved
      customFields: [
        {
          key: '1678228069813',
          value: 'this is a secret?'
        },
        {
          key: '1678228069823',
          value: 'Hamilton'
        },
        {
          key: keyE,
          value: [blobDocument, blobPhotoA, blobPhotoB]
        }
      ]
    },
    'returns the correct details on the admin profile'
  )

  kaitiaki.close()
})

/*
NOTE: this is a test for a bug im getting in the frontend when a member
creates a group profile with custom fields.

Steps to reproduce:
- A member creates a profile with custom fields and it fails...
- Only happening on create, not update

Error: Unknown groupId

Problem discovered:
- Its trying to create the admin profile too, but cannot because it cant

*/
test('member can create a profile with custom fields', async t => {
  t.plan(10)
  // t.end()

  /**
   * SETUP
   */
  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  const initGroup = InitGroup(kaitiaki.ssb)
  const savePersonMember = SavePerson(member.apollo)
  const savePersonKaitiaki = SavePerson(kaitiaki.apollo)

  const getPersonMember = GetPerson(member.apollo, getPersonQuery)
  const getPersonKaitiaki = GetPerson(kaitiaki.apollo, getPersonQuery)

  const getPerson = async (profileId, getter) => {
    const res = await getter(profileId)
    t.error(res.errors, 'gets person without error')
    return res.data.person
  }

  /**
   * TESTS
   */

  // 1. admin creates a group
  const communityInput = {
    preferredName: 'Smoothie Club',
    // these are the custom field definitions
    customFields: [
      {
        key: '1678228069813',
        label: 'secret',
        type: 'text',
        required: false,
        visibleBy: 'admin',
        tombstone: null
      },
      {
        key: '1678228069823',
        label: 'hometown',
        type: 'text',
        required: false,
        visibleBy: 'members',
        tombstone: null
      }
    ]
  }
  const { groupId } = await initGroup(communityInput)

  // 2. the kaitiaki invites the member to join
  // NOTE: the member doesnt need any profiles in the group
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.ssb.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)
  // wait a second because the db rebuilds when replicated to the member
  await sleep(1000)

  // 3. member attempts to create a profile
  let profileInput = {
    type: 'person',
    authors: {
      add: [
        '*'
      ]
    },
    customFields: [
      // NOTE: the UI doesnt allow non-kaitiaki to see and save changes to admin custom fields
      // only member custom fields
      {
        key: '1678228069823',
        value: 'Hamilton'
      }
    ],
    recps: [groupId]
  }

  let res = await savePersonMember(profileInput)
  t.error(res.errors, 'saves person without error')

  // 2. member gets the profile
  const profileId = res.data.savePerson

  let profile = await getPerson(profileId, getPersonMember)

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: null, // expected to be null because this is already an adminProfile itself
      recps: [groupId],

      // we expect all of the custom fields to be saved
      customFields: [
        {
          key: '1678228069823',
          value: 'Hamilton'
        }
      ]
    },
    'returns only the group profile and no admin profile'
  )
  // replicate it to the kaitiaki
  await member.replicate(kaitiaki)

  // 3. kaitiaki gets the profile
  profile = await getPerson(profileId, getPersonKaitiaki)

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: null, // expected to be null because this is already an adminProfile itself
      recps: [groupId],

      // we expect all of the custom fields to be saved
      customFields: [
        {
          key: '1678228069823',
          value: 'Hamilton'
        }
      ]
    },
    'returns only the group '
  )

  // 4. the kaitiaki saves changes to the profile
  profileInput = {
    id: profileId,
    customFields: [
      // the kaitiaki saves an admin only field
      {
        key: '1678228069813',
        value: 'Cherese ate the cookies from the cookie jar'
      }
    ]
  }

  res = await savePersonKaitiaki(profileInput)
  t.error(res.errors, 'saves perosn without error')

  // replicate it to the member
  await kaitiaki.replicate(member)

  // 5. kaitiaki gets the profile
  profile = await getPerson(profileId, getPersonKaitiaki)
  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: {
        customFields: [
          {
            key: '1678228069813',
            value: 'Cherese ate the cookies from the cookie jar'
          }
        ]
      },
      recps: [groupId],

      // we expect all of the custom fields to be saved
      customFields: [
        {
          key: '1678228069823',
          value: 'Hamilton'
        }
      ]
    },
    'kaitiaki minted a new admin profile with the custom fields'
  )

  // 6. member gets the updated profile
  profile = await getPerson(profileId, getPersonMember)
  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: null,
      recps: [groupId],

      // we expect all of the custom fields to be saved
      customFields: [
        {
          key: '1678228069823',
          value: 'Hamilton'
        }
      ]
    },
    'member doesnt see the admin profile'
  )

  kaitiaki.close()
  member.close()
})
