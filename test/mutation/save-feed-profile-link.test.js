const test = require('tape')
const TestBot = require('../test-bot')
const { isMsgId } = require('ssb-ref')

function Save (apollo) {
  return async function saveLink (linkDetails) {
    return apollo.mutate({
      mutation: `mutation($linkDetails: FeedProfileLinkInput!) {
        saveFeedProfileLink(input: $linkDetails)
      }`,
      variables: { linkDetails }
    })
  }
}

test('saveFeedProfileLink (create)', async (t) => {
  t.plan(6)
  const { ssb, apollo } = await TestBot()
  const saveLink = Save(apollo)

  ssb.tribes.create({}, (_, { groupId }) => {
    /* create public profile */
    ssb.profile.publicCommunity.create({ authors: { add: [ssb.id] } }, async (err, profilePublic) => {
      if (err) throw err

      const result = await saveLink({ profile: profilePublic })
      t.error(result.errors, 'query should not return errors')
      const linkId = result.data.saveFeedProfileLink
      t.true(isMsgId(linkId), 'saves link, returning linkId')

      /* create a private profile */
      const details = {
        authors: {
          add: [ssb.id]
        },
        recps: [groupId]
      }
      ssb.profile.privateCommunity.create(details, async (err, profile) => {
        if (err) throw err

        const result = await saveLink({ profile })
        t.error(result.errors, 'query should not return errors')
        const linkId = result.data.saveFeedProfileLink
        t.true(isMsgId(linkId), 'saves link, returning linkId')
        ssb.db.getMsg(linkId, (_, msg) => {
          t.true(msg.meta.private, 'is encrypted')
          t.deepEqual(msg.value.content.recps, [groupId], 'link encrypted to group')

          ssb.close()
        })
      })
    })
  })
})

test('saveFeedProfileLink (create, with recps-guard)', async (t) => {
  t.plan(2)
  const { ssb, apollo } = await TestBot({ recpsGuard: true })
  const saveLink = Save(apollo)

  const details = {
    authors: {
      add: [ssb.id]
    },
    allowPublic: true
  }

  ssb.tribes.create({}, (_, { groupId }) => {
    ssb.profile.publicPerson.create(details, async (err, profile) => {
      if (err) throw err

      await saveLink({ profile })
        .catch(err => t.true(err, 'plain query should error'))

      const result2 = await saveLink({ profile, allowPublic: true })
      t.error(result2.errors, 'allowPublic: true means successful publish')

      ssb.close()
    })
  })
})
