const blobToURI = require('ssb-serve-blobs/id-to-url')
const CustomField = require('graphql-custom-field')
const get = require('lodash.get')

const sortAscending = require('../src/lib/sort-ascending')
const ssbResolvers = require('./ssb')

module.exports = function Resolvers (sbot) {
  const {
    getProfile,

    // getPersons,
    getTribes,
    getSubtribes,
    getTribe,
    getConnectedPeers,
    getPatakas,
    getAdminProfile,

    findPersons,
    listPerson,

    postSavePerson,
    postTombstonePerson,
    postSaveCommunity,
    postSavePataka,
    postSaveProfileLink,

    gettersWithCache
  } = ssbResolvers(sbot)

  const resolvers = {
    CustomField, // Scalar
    Query: {
      whoami: (_, __, context) => {
        const whoami = {
          public: context.public
        }
        if (context.personal) whoami.personal = context.personal

        return whoami
      },
      // persons: async () => getPersons(),
      person: (_, { id }) => getProfile(id),
      community: (_, { id }) => getProfile(id),
      pataka: (_, { id }) => getProfile(id),
      profile: (_, { id }) => getProfile(id),

      tribes: () => getTribes(),
      subtribes: (_, { id }) => getSubtribes(id),
      tribe: (_, { id }) => getTribe(id),
      connectedPeers: () => getConnectedPeers(),
      patakas: () => getPatakas(),

      findPersons: (_, { name, type, groupId }) => findPersons(name, { type, groupId }),
      // listPerson: (_, { type, groupId }) => listPerson(type, groupId)
      listPerson: (_, { type, groupId }) => {
        return listPerson(type, groupId)
      }
    },
    Mutation: {
      savePerson: (_, { input }) => postSavePerson(input),
      saveCommunity: (_, { input }) => postSaveCommunity(input),
      savePataka: (_, { input }) => postSavePataka(input),
      saveGroupProfileLink: (_, { input }) => postSaveProfileLink(input),
      saveFeedProfileLink: (_, { input }) => postSaveProfileLink(input),
      deletePerson: (_, { id, tombstoneInput }) => postTombstonePerson(id, tombstoneInput)
    },
    Profile: {
      __resolveType (profile, context, info) {
        switch (profile.type) {
          case 'person': return 'Person'
          case 'person/source': return 'Person'
          case 'person/admin': return 'Person'
          case 'community': return 'Community'
          case 'pataka': return 'Pataka'
          default: return null
        }
      }
    },
    Community: {
      allowWhakapapaViews: (profile) => profile.allowWhakapapaViews !== false,
      allowPersonsList: (profile) => profile.allowPersonsList !== false,
      allowStories: (profile) => profile.allowStories !== false,
      acceptsVerifiedCredentials: (profile) => profile.acceptsVerifiedCredentials || false,
      issuesVerifiedCredentials: (profile) => profile.issuesVerifiedCredentials || false,
      customFields: (profile) => {
        if (!profile.customFields) return null

        return Object.entries(profile.customFields)
          .map(([key, value]) => ({ key, ...value }))
          .sort(sortAscending)
      }
    },
    Person: {
      adminProfile: async (groupProfile) => {
        if (!groupProfile.recps) return null // public profile

        const groupId = groupProfile.recps[0]
        if (groupId.startsWith('ssb:identity/po-box')) return null // already looking at an admin profile

        return getAdminProfile(groupId, groupProfile)
      },
      customFields: (profile) => {
        if (!profile.customFields) return []

        return Object.entries(profile.customFields).map(([key, value]) => {
          if (get(value, 'type') === 'date') {
            return { key, value: get(value, 'value'), type: 'date' }
          }
          return { key, value }
        })
      }
    },
    PublicIdentity: {
      profile: ({ profileId }) => getProfile(profileId)
    },
    PersonalIdentity: {
      profile: ({ profileId }) => getProfile(profileId)
    },
    TribeFaces: {
      subtribes: (parent) => getSubtribes(parent.id)
    },
    Image: {
      uri (image) {
        let unbox = image.unbox

        // NOTE some legacy images had unbox stored in uri
        if (!unbox && image.uri) unbox = new URL(image.uri).searchParams.get('unbox')

        if (unbox) unbox = unbox.replace('.boxs', '')

        // TODO pivot on type - could be hyperBlobs?
        const port = sbot.config.serveBlobs && sbot.config.serveBlobs.port
        return blobToURI(image.blob, { unbox, port })
      }
    },
    CommunityCustomFieldInterface: {
      __resolveType (customField) {
        switch (customField.type) {
          case 'list': return 'CommunityCustomFieldList'
          case 'file': return 'CommunityCustomFieldFile'
          default: return 'CommunityCustomField'
        }
      }
    },
    PersonCustomFieldInterface: {
      __resolveType (customField) {
        switch (get(customField, 'type')) {
          case 'date': return 'PersonCustomFieldDate'
          default: return 'PersonCustomField'
        }
      }
    }
  }

  // I don't like this hack, but we need a way to instantiate the gettersWithCache
  // exactly once, otherwise we're running duplicate caches + listeners

  return {
    resolvers,
    gettersWithCache
  }
}
