const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { GraphQLError } = require('graphql')

module.exports = function GetSubtribes (sbot, getTribe) {
  return function getSubtribes (groupId, cb) {
    sbot.tribes.findSubGroupLinks(groupId, (err, links) => {
      if (err) return cb(new GraphQLError(err))

      pull(
        pull.values(links),
        pull.filter(link => !link.admin),
        pull.map(link => link.subGroupId),
        pull.filter(Boolean),
        paraMap(getTribe, 6),
        pull.collect(cb)
      )
    })
  }
}
