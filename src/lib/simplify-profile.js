module.exports = function simplifyProfile (profile) {
  const state = {
    id: profile.key,
    type: profile.type.replace('profile/', '')
  }
  for (const field in profile) {
    if (field === 'key') continue
    if (field === 'type') continue
    if (field === 'states') continue
    if (field !== 'tombstone' && isEmptyString(profile[field])) continue
    state[field] = profile[field]
  }

  return state
}

function isEmptyString (input) {
  if (input === null) return true
  if (input === '') return true
  return false
}
