const { GraphQLError } = require('graphql')

module.exports = function GetRootMsg (sbot) {
  return function (id, cb) {
    sbot.get({ id, private: true, meta: true }, (err, msg) => {
      if (err) return cb(new GraphQLError(err))

      const type = msg.value.content.type

      cb(null, {
        id,
        type,
        isPrivate: Boolean(msg.value.content.recps), // no recps means its a public profile
        isPerson: type === 'profile/person',
        isCommunity: type === 'profile/community'
      })
    })
  }
}
