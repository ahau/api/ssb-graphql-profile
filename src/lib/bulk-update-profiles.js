const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const spec = require('ssb-profile/spec')
const pick = require('lodash.pick')
const get = require('lodash.get')
const { GraphQLError } = require('graphql')

const getTypePath = require('./get-type-path')

module.exports = function BulkUpdateProfiles (ssb) {
  return function bulkUpdateProfiles (profiles, allDetails, cb) {
    if (allDetails.recps) return cb(new GraphQLError('unexpected recps field found while bulk updating personal profile'))
    // NOTE: ensure we cannot delete all associated profiles
    if (allDetails.tombstone) return cb(new GraphQLError('You cannot bulk tombstone all your profiles'))
    // NNOTE: ensure we cannot bulk update custom fields on all profiles
    if (allDetails.customFields) delete allDetails.customFields

    pull(
      pull.values([...profiles.private, ...profiles.public]),
      paraMap(
        (profile, cb) => magicUpdate(profile, allDetails, cb),
        5 // how many to run in parallel
      ),
      pull.collect(cb)
    )
  }
  function magicUpdate (profile, allDetails, cb) {
    const typePath = getTypePath(profile.type, profile.recps)
    const crut = get(ssb.profile, typePath)
    if (!crut || !crut.update) return cb(new GraphQLError(`unknown profile type ${profile.type}`))

    const details = getCrutDetails(allDetails, typePath)
    if (Object.keys(details).length === 0) return cb(null, null)
    // don't publish an update if there is nothing to save

    if (!profile.recps) details.allowPublic = true
    crut.update(profile.key, details, cb)
  }
}

function getCrutDetails (allDetails, typePath) {
  const specProps = Object.keys(get(spec, [...typePath, 'props']))

  return pick(allDetails, [...specProps, 'authors', 'tombstone'])
}
