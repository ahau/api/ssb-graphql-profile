module.exports = function sortAscending (A, B) {
  if (!A.order) return 1
  if (!B.order) return -1

  if (A.order === B.order) {
    return 0
  }

  return A.order < B.order ? -1 : 1
}
